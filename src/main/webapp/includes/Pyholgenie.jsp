<h1 class="fw-300" ,>Phylogenie</h1>
<p>
    Phylogenie, oftewel de leer van evolutie, bestaat uit zowel evolutionaire historie als relaties tussen individuen of
    groepen organismen. Om deze relaties te kunnen onderzoeken kunnen twee technieken worden gebruikt: morfologisch, of
    moleculair onderzoek.
    <br><br>
    Morfologisch onderzoek houdt in dat relaties worden gebaseerd op morfologische (uiterlijke) kenmerken, bijvoorbeeld
    lengte, haarkleur, of het hebben van vleugels. Een van de nadelen van deze manier van onderzoek is dat er erg veel
    metingen nodig zijn om een representatief gemiddelde te krijgen voor een specifieke soort of populatie. Daarnaast
    zijn er oneindig veel kenmerken die onderzocht kunnen worden, en het is niet zeker dat een uiterlijk kenmerk
    evolutionair gezien een relatie heeft. Zo hebben een vogel en vleermuis allebei vleugels en kunnen ze hiermee
    vliegen, maar zijn zij helemaal niet dicht aan elkaar gerelateerd; de ene is een zoogdier, de ander een reptiel.
    <br><br>
    Bij moleculair onderzoek wordt er gekeken naar sequenties van organismes. Dit kunnen DNA sequenties zijn, maar het
    zou
    ook met RNA of zelfs eiwitten sequenties kunnen. Voor deze methode wordt gebruik gemaakt van Multiple Sequence
    Alignment (MSA). MSA wordt in het tabblad hieronder verder uitgelegd. Gaped MSA geeft de mogelijkheid om
    veel verschillende bomen te maken die allemaal een andere interpretatie van <span class="tooltip" id="mutatie">mutaties
        <span class="tooltiptext">een spontane verandering in het DNA</span></span>
    (weergegeven als mis-alignementen or gaps). Zo kan een gap kan betekenen dat de sequentie met een gap een
    <span class="tooltip" id="deletie">deletie
        <span class="tooltiptext"> een mutatie waarbij er een of meerdere nucleotiden missen uit het DNA</span></span>
    heeft, maar het zou ook kunnen dat de sequentie zonder gap juist een <span class="tooltip" id="insertie">insertie
        <span class="tooltiptext" id="insertie-text">een mutatie waarbij er een of meerdere nucleotiden zijn toegevoegd aan het DNA</span></span>
    heeft. Als je al deze verschillen gaat meenemen kun je wel zien dat het al snel aan heel erg veel mogelijke bomen
    zit.
    <br><br>

</p>