<%--
  Created by IntelliJ IDEA.
  User: Ketos
  Date: 5-12-2017
  Time: 08:52
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MSA webcursus</title>
    <meta name="description" content="">
    <meta name="author" content="ink, cookbook, recipes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">


    <!-- load CSS -->
    <link rel="stylesheet" type="text/css" href="./css/ink.css">
    <link rel="stylesheet" type="text/css" href="./css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="./css/MSA-elements.css">
    <link rel="stylesheet" type="text/css" href="./lib/treant-js/Treant.css">
    <link rel="stylesheet" type="text/css" href="./css/tooltips.css">

    <!-- load Ink's CSS for IE8 -->
    <!--[if lt IE 9 ]>
    <link rel="stylesheet" href="./css/ink-ie.min.css" type="text/css" media="screen" title="no title" charset="utf-8">
    <![endif]-->

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <link rel="stylesheet" type="text/css" href="./css/ink.min.css">
    <!-- Load the js files -->
    <script type="text/javascript" src="http://fastly.ink.sapo.pt/3.1.10/js/ink-all.js"></script>
    <!-- Autoload is required for the examples on this page. Read more below. -->
    <script type="text/javascript" src="http://fastly.ink.sapo.pt/3.1.10/js/autoload.js"></script>
    <script type="text/javascript" src="webFrameworks/ink-3.1.10/js/ink.tabs.js"></script>

</head>

<body>

<div class="ink-grid">

    <!--[if lt IE 9 ]>
    <div class="ink-alert basic" role="alert">
        <button class="ink-dismiss">&times;</button>
        <p>
            <strong>You are using an outdated Internet Explorer version.</strong>
            Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
        </p>
    </div>
    <![endif]-->

    <!-- Add your site or application content here -->

    <header class="vertical-space">
        <img src="graphics/Bioinf_logo.gif" alt="">
        <h1>Bio-informatica Webcursus
            <small>Multiple sequence alignment</small>
        </h1>
        <nav class="ink-navigation">
            <ul class="menu horizontal black">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">item</a></li>
                <li><a href="#">item</a></li>
            </ul>
        </nav>
    </header>
    <div class="column-group gutters">
        <div class="all-100">
            <h2>Interactive MSA</h2>
            <h3></h3>
            </br>
            <!-- replace 'top' with 'bottom', 'left' or 'right' to reposition navigation -->
            <div id="Main_menu" class="ink-tabs left" data-prevent-url-change="true">
                <!-- If you're using 'bottom' positioning, put this div AFTER the content. -->
                <ul class="tabs-nav">
                    <li><a class="tabs-tab" href="#home">Home</a></li>
                    <li><a class="tabs-tab" href="#DNA_mRNA_eiwiten">DNA, mRNA en eiwitten</a></li>
                    <li><a class="tabs-tab" href="#eiwit_in_meer_soorten">Zelfde eiwit in meerdere soorten</a></li>
                    <li><a class="tabs-tab" href="#Phylogenie">Phylogenie</a></li>
                    <li><a class="tabs-tab" href="#Allingment">Allignment</a></li>
                    <li><a class="tabs-tab" href="#Phylogenietiche_bomen">Phylogenetische bomen</a></li>
                    <li><a class="tabs-tab" href="#Verassende_gelijkenissen">Verrassende gelijkenissen uit de
                        praktijk</a>
                    </li>
                    <li><a class="tabs-tab" href="#Alles_samen">Alles samen en probeer het zelf</a></li>
                </ul>

                <!-- Now just place your content -->
                <div id="home" class="tabs-content">
                    <h1 class="fw-300">Home</h1>
                    <p>
                        Welkom op de webcursus voor MSA
                    </p>
                </div>
                <div id="DNA_mRNA_eiwiten" class="tabs-content">
                    <h1 class="fw-300">DNA, mRNA en eiwitten</h1>
                    <p>
                        Hier wordt uitgelegd wat ieder onderdeel is en wat het verschil er tussen in naast nog het
                        belang
                    </p>
                </div>
                <div id="eiwit_in_meer_soorten" class="tabs-content">
                    <h1 class="fw-300">Zelfde eiwit in meerdere soorten</h1>
                    <p>
                        bijvoorbeeld huishoudgenen
                    </p>
                </div>
                <div id="Phylogenie" class="tabs-content">
                    <jsp:include page="./includes/Pyholgenie.jsp"/>
                </div>
                <div id="Allingment" class="tabs-content">
                    <jsp:include page="includes/Alignment.jsp"></jsp:include>
                </div>
                <div id="Phylogenietiche_bomen" , class="tabs-content">
                    <h1 class="fw-300">Phylogenetische bomen</h1>
                    <p>voorbeeld van hoe je bomen bouwt</p>
                </div>
                <div id="Verassende_gelijkenissen" class="tabs-content">
                    <h1 class="fw-300">Verrassende gelijkenissen uit de praktijk</h1>
                    <p>hier komen verrassende voorbeelden, waarin te zien is dat we soms toch dichter bij andere
                        organismes
                        liggen dan men in eerst instantie zou denken</p>
                </div>
                <div id="Alles_samen" class="tabs-content">
                    <h1 class="fw-300">Alles samen en probeer het zelf</h1>
                    <div id="uitleg">
                        <p>
                            De sequenties die hieronder staan, kunnen heen en weer geschoven worden door er met de muis
                            op te gaan staan en terwijl de muiskop wordt vastgehouden de muis heen en weer beweegt.<br>
                            Iedere sequentie hoort bij een organisme die wordt weergegeven in de phylogenetische boom
                            onder de sequenties.<br>
                            van boven naar beneden: Mens, Aap, Schildpad, Vogel, Bacterie
                        </p>
                    </div>
                    <div id="seqs" class="dragv">
                    </div>
                    <div id="ChangingTree" style="width:800px; height: 500px"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="clearfix">
    <div class="ink-grid">
        <ul class="unstyled inline half-vertical-space">
            <li class="active"><a href="#">About</a></li>
            <li><a href="#">Sitemap</a></li>
            <li><a href="#">Contacts</a></li>
        </ul>
        <p class="note">Identification of the owner of the copyright, either by name, abbreviation, or other designation
            by which it is generally known.</p>
    </div>
</footer>
<script type="application/javascript" src="./lib/treant-js/vendor/raphael.js"></script>
<script type="application/javascript" src="./lib/treant-js/Treant.js"></script>
<script type="application/javascript" src="./lib/newick-js/src/newick.js"></script>
<script type="application/javascript" src="js/InteractivMSA.js"></script>

</body>
</html>
