$(document).ready(function () {
    /**
     * following this bit there al a lot of variable and bits of code that are needed throughout the code.
     * each will have al little expectation of what is is and where its needed for as java doc code.
     */

    /**
     * a dictionary style array that includes the sequences that need to be display with extra parameters that will change throughout
     */
    var seqs = {
        "Mens": {
            "seq": "MPYIVDVYAREVLDSRGNPTVEVEVYTETGAFGRALVPSGASTGEYEAVELRDGDKDRYLGKGVLTAVNNVNEII", // Sequence
            "length": 0, // Length including offset
            "styled": [], // HTML-styled view of the sequence
            "pos": 0
        },
        "Aap": {
            "seq": "EVLDSRGNPTVEVEVYTETGAFGRALVPSGASTGEYEAVELRDGDKDRYLGKGVLTAVNNVMPYIVDVYARNEII",
            "length": 0,
            "styled": [], // HTML-styled view of the sequence
            "pos": 0
        },
        "schildpad": {
            "seq": "MPYRVSRYAREVYTETGAFLGKGVLTAVGRALVPSGASTGEYEAVELRDVLDSRGNPTVEVEGDKDRYNNVNEII",
            "length": 0,
            "styled": [], // HTML-styled view of the sequence
            "pos": 0
        },
        "vogel": {
            "seq": "MVGYIEREVYTEDDLTAVGRALVPSGASTGEYEAVELRDVLDSRGNPTVEVEGDKDRYNVNEIITGAFLGKGVEI",
            "length": 0,
            "styled": [], // HTML-styled view of the sequence
            "pos": 0
        },
        "bacterie": {
            "seq": "MPYIVDSGASTGEYEAVELRDGDKDRVYAREVLDSRGNPTVEVEVYTETGALGKGVLTAVNNVNEIIFGRALVPY",
            "length": 0,
            "styled": [],
            "pos": 0
        },
        // "Muis": {
        //     "seq": "IIENRAYIVDVYEVLDSRGNPTVEVEVYTETGAFGRALVPSGASTGEYEAVELRDGDKDRYLGKGVLTAVNNVMP",
        //     "length": 0,
        //     "styled": [],
        //     "pos": 0
        // }
    };

    /**
     * a string array that includes a matrix with the scoring for the alignment will later be converted into an actual matrix
     *
     */
    let matrix = ["A  R  N  D  C  Q  E  G  H  I  L  K  M  F  P  S  T  W  Y  V",
        "A -5  2  2  2  1  1  1 -0  2  2  2  1  1  3  1 -1 -0  3  2 -0",
        "R  2 -6  1  2  4 -1  1  3 -0  3  3 -2  2  4  2  1  1  4  3  3",
        "N  2  1 -6 -1  3 -0  1  1 -0  4  4 -0  3  4  3 -0 -0  4  3  4",
        "D  2  2 -1 -6  4  1 -1  2  2  4  5  1  4  4  2  1  1  6  4  4",
        "C  1  4  3  4 -9  4  5  4  4  2  2  4  2  3  4  2  1  3  3  1",
        "Q  1 -1 -0  1  4 -6 -2  2 -1  3  3 -1 -0  4  2 -0  1  3  2  3",
        "E  1  1  1 -1  5 -2 -6  3 -0  4  4 -1  2  4  2 -0  1  4  3  3",
        "G -0  3  1  2  4  2  3 -6  3  5  4  2  4  4  3  1  2  4  4  4",
        "H  2 -0 -0  2  4 -1 -0  3 -8  4  3  1  2  2  3  1  2  3 -2  4",
        "I  2  3  4  4  2  3  4  5  4 -5 -1  3 -1  1  4  3  1  3  2 -3",
        "L  2  3  4  5  2  3  4  4  3 -1 -4  3 -2 -0  3  3  2  2  2 -1",
        "K  1 -2 -0  1  4 -1 -1  2  1  3  3 -5  2  4  1  1  1  4  3  3",
        "M  1  2  3  4  2 -0  2  4  2 -1 -2  2 -6 -0  3  2  1  2  2 -1",
        "F  3  4  4  4  3  4  4  4  2  1 -0  4 -0 -6  4  3  2 -0 -3  1",
        "P  1  2  3  2  4  2  2  3  3  4  3  1  3  4 -8  1  2  5  4  3",
        "S -1  1 -0  1  2 -0 -0  1  1  3  3  1  2  3  1 -5 -1  4  2  2",
        "T -0  1 -0  1  1  1  1  2  2  1  2  1  1  2  2 -1 -5  4  2 -0",
        "W  3  4  4  6  3  3  4  4  3  3  2  4  2 -0  5  4  4 11 -2  3",
        "Y  2  3  3  4  3  2  3  4 -2  2  2  3  2 -3  4  2  2 -2 -7  2",
        "V -0  3  4  4  1  3  3  4  4 -3 -1  3 -1  1  3  2 -0  3  2 -4"];

    /**
     * a variable needed to store the converted matrix for above
     * @type {Array}
     */
    let scoreMatrix = [];

    /**
     * code that will transform the matrix variable into a matix that is then stored into scoreMatrix.
     */
        // this converts the array above into a matrix
    let cols = matrix[0].split(/\s+/);
    for (let i = 1; i < matrix.length; i++) {
        let cells = matrix[i].split(/\s+/);
        //go through cells in this row
        for (let j = 1; j < cols.length + 1; j++) {
            scoreMatrix[cells[0] + cols[j - 1]] = parseInt(cells[j]);
        }
    }

    /**
     * variable that are made globle in order to save a lot of passing troug to functions and make the available for all
     */
    let nseqs = Object.keys(seqs).length;
    let longest = 0;
    let scores = {};
    /**
     * A parameter that is the amount of horizontal pixels it takes to display a character in monospace
     */
    let gridsize = 8;
    /**
     * This parameter might need changing based on the resolution of the screen. but it reverences
     * the starting position of all sequence's
     */
    let standaardAfwijking = 612;

    /**
     * Variables that are needed to make a list of all the unique combinations of the sequences.
     * This is in order to not calculate seq5 vs seq1 when you have seq1 vs seq5 or example.
     * Since they have the same score. The two related functions are given below these Variables.
     */
    let seq_list = [];
    let combos = [];
    /**
     *  function for making a list of all the elements inside a dict.
     */
    $.each(seqs, function (key) {
        seq_list.push(key);
    });
    /**
     * function for making unique combinations
     */
    for (let i = 0; i < seq_list.length - 1; i++) {
        // This is where you'll capture that last value
        for (let j = i + 1; j < seq_list.length; j++) {
            combos.push(seq_list[i] + ',' + seq_list[j]);
        }
    }


    /**
     * Adds the correct amount of postfix dashes to each sequences
     */
    function postfix() {
        $.each(seqs, function (key, value) {
            longest = msa_length();
            let sequence_length = value.length + $("#Gap_pre_" + key).text().length;
            //make sure that the new longest has no post items
            if (sequence_length > longest) {
                $("#Gap_post_" + key).text("")
            }
            //Smaller or equal in order to make sure that the longest element get's an updated as well.
            if (sequence_length <= longest) {
                //Clear the previous state
                $("#Gap_post_" + key).text("");
                $("#Gap_post_" + key).css("margin-left", $("#Gap_pre_" + key).text().length * gridsize);
                $("#Gap_post_" + key).text('-'.repeat(longest - sequence_length));
            }
        });
    }

    /**
     *Takes a look at the current position of the sequence that is being moved and look if prefix dashes need to be added.
     * @param sequence
     */
    function prefix(sequence) {
        //calculate the character posison the sequences is.
        let id = sequence[0].attributes.id.value;
        let charIndex = ((sequence.offset().left - standaardAfwijking) / gridsize);
        if (charIndex >= 0) {
            let preGapAmount = '-'.repeat(charIndex);
            $("#Gap_pre_" + id).text(preGapAmount);
        }
    }

    /**
     *Look at all the lengths of sequences including there postfix length in order to determine the size of the longest sequence
     * @returns {number}
     */
    function msa_length() {
        longest = 0;
        $.each(seqs, function (id, sequence) {
            let sl = sequence.length + $("#Gap_pre_" + id).text().length;
            longest = sl > longest ? sl : longest;
        });
        return (longest);
    }

    /**
     * look at the occurrences of letter and checks if letters in the same position has a majority occurrence in the sequences
     * if that is the case send the letter and position to the matcher function
     * Executes: msa_length and matcher
     *
     */
    function align() {
        let max_length = msa_length();
        let column = {};
        let major = "";
        for (let i = 0; i < max_length; i++) {
            // Calculate occurrences for every letter
            // Input: GATCCA
            // column: {'A': 2, 'C': 2, 'G':1, 'T': 1}
            $.each(seqs, function (key) {
                let line = $("#Gap_pre_" + key).text() + seqs[key]['seq'] + $("#Gap_post_" + key).text();
                let letter = line[i];
                column[letter] = column[letter] ? column[letter] + 1 : 1;
            });

            // Check if the letter occurs > half the number of sequences (majority)
            $.each(column, function (letter, count) {
                letter = letter !== 'undefined' ? letter : '-';
                if (count > nseqs / 2) {
                    // Perform matching given the majority character
                    matcher(letter, i);
                }
            });

            // reset
            major = "";
            column = {};
        }
    }

    /**
     * Compare each letter for a column with the majority letter and highlights accordingly
     * @param major
     * @param position
     *
     * Results in a string with HTML styled characters
     */
    function matcher(major, position) {
        $.each(seqs, function (key, value) {
            //get the length of the prefix
            let prefix_length = $("#Gap_pre_" + key).text().length;
            // Check if match is in the sequence
            if (position === 0 || position >= prefix_length) {
                // Calculate actual position *within* the sequence
                let seq_pos = position - prefix_length;
                // Construct string with styled <span> elements
                let pre = styled = post = [];
                if (value['seq'][seq_pos] === major) {
                    // Only add a pre-fix (unstyled letter(s)) if match > position 0
                    if (seq_pos !== 0) {
                        pre = value['styled'].slice(0, seq_pos);
                    }
                    let match = "<span class=amino_" + major + ">" + major + "</span>";
                    let post = value['styled'].slice(seq_pos + 1, value['seq'].length);
                    // Combine elements
                    styled = styled.concat(pre);
                    styled = styled.concat(match);
                    styled = styled.concat(post);


                    // Add to <div> as html
                    $("#" + key).html(styled.join(""));
                    // Store including styling
                    seqs[key]['styled'] = styled;
                }
            }
        });
    }

    /**
     * Strips all HTML tags from a string
     * needed in order to recolor the string for the current alignment
     * @param html
     * @returns {string | string} = ""
     */
    function html_strip(html) {
        let tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    }


    /**
     *Calculates the alignment scores by using the scoreMatrix of all unique sequence combinations and adds it to a scores variable
     */
    function calculate_score() {
        $.each(combos, function (combo) {
            let ref_keys = combos[combo].split(",");
            let seq1_with_offset = $("#Gap_pre_" + ref_keys[0]).text() + seqs[ref_keys[0]].seq;
            let seq2_with_offset = $("#Gap_pre_" + ref_keys[1]).text() + seqs[ref_keys[1]].seq;
            let totscore = 0;
            for (let i = 0; i < ((seq1_with_offset.length < seq2_with_offset.length) ? seq1_with_offset.length : seq2_with_offset.length); i++) {
                let score = scoreMatrix[seq1_with_offset[i] + seq2_with_offset[i]];

                totscore += (score === undefined) ? 0 : score;
            }
            scores[ref_keys[0] + "_" + ref_keys[1]] = totscore;

        })
    }

    /**
     * make a distance matrix from all the unique combination with there alignment scores needed for UPGMA
     * @param labels
     * @returns {Array}
     */
    function makeDistanceMatrix(labels) {

        let mat = [];
        let a, b;
        for (let i = 0; i < labels.length; i++) {
            let row = [];
            for (let j = 0; j < labels.length; j++) {
                a = i;
                b = j;
                if (b < a)
                    [a, b] = [b, a];
                let key = scores[labels[a] + '_' + labels[b]];
                row.push(key);
            }
            mat.push(row);
        }

        for (let i = 0; i < labels.length; i++) {
            mat[i] = mat[i].splice(0, i);
        }
        return mat
    }

    /**
     * Locates the smallest cell in the table
     * @param table Pairwise alignment scoring table
     * @returns {{x: number, y: number}}
     */
    function lowest_cell(table) {
        let min_cell = Infinity;
        let x, y;
        x = y = -1;

        for (let i = 0; i < table.length; i++) {
            for (let j = 0; j < table[i].length; j++) {
                if (table[i][j] < min_cell) {
                    min_cell = table[i][j];
                    x = i;
                    y = j;
                }
            }
        }
        return {x: x, y: y};
    }

    /**
     * Combines two labels in a list of labels
     * @param labels
     * @param a Index of label A
     * @param b Index of label B
     * @returns {*}
     */
    function join_labels(labels, a, b) {
        // Swap if the indices are not ordered
        if (b < a) {
            [a, b] = [b, a];
        }
        labels[a] = "(" + labels[a] + "," + labels[b] + ")";
        labels.splice(b, 1);

        return labels;
    }


    /**
     * Joins the entries of a table on the cell (a, b) by averaging their data entries
     * @param table
     * @param a Index of cell A
     * @param b Index of cell B
     * @returns {*}
     */
    function join_table(table, a, b) {
        // Swap if the indices are not ordered
        if (b < a) {
            [a, b] = [b, a];
        }

        // For the lower index, reconstruct the entire row (A, i), where i < A
        let row = [];
        for (let i = 0; i < a; i++)
            row.push((table[a][i] + table[b][i]) / 2);
        table[a] = row;

        // Then, reconstruct the entire column (i, A), where i > A
        //   Note: Since the matrix is lower triangular, row b only contains values for indices < b
        for (let i = a + 1; i < b; i++)
            table[i][a] = (table[i][a] + table[b][i]) / 2;

        // We get the rest of the values from row i
        for (let i = b + 1; i < table.length; i++) {
            table[i][a] = (table[i][a] + table[i][b]) / 2;
            // Remove the (now redundant) second index column entry
            table[i].splice(b, 1);
        }

        table.splice(b, 1);
        return table;

    }

    /**
     * Runs the UPGMA algorithm on a labelled table
     * @param table
     * @param labels
     * @returns {*}
     * @constructor
     */
    function UPGMA(table, labels) {
        while (labels.length > 1) {
            let coords = lowest_cell(table);
            table = join_table(table, coords.x, coords.y);
            labels = join_labels(labels, coords.x, coords.y);
        }
        return labels;
    }

    /**
     * Runs a list of functions and operations that need to be done in order,
     * and are repeated on multiple places in the code(initial set up. drag and stop).
     *
     * Preparation Work
     * - generate a slice of a list of the sequence keys.  this will be transformed into newick format
     *
     * Functions included are:
     *
     * - align: aligns the sequences for details look at the functions description.
     * - calculate score: calculate the scores of the current alignment for details look at the functions description.
     * - makeDistanceMatrix: makes a distance matrix for details look at the functions description.
     * - UPGMA: runs the UPGMA algorithm for details look at the functions description.
     * - Newick.parse: function of a js lib (newick-js) for the orginal scrip look at https://github.com/jasondavies/newick.js/
     *                 a lot of chages have been made to it in order to genrate the correct output that can be used by the Treant function.
     * - Treant:function of a js lib (treant-js) for details look at http://fperucic.github.io/treant-js/.
     *
     */
    function Work() {
        // make a copy of the list with all sequences, this need to be repeated since the copy wil end op being the upgma result
        let labels = seq_list.slice();
        //make the alignment for the current position of the sequences
        align();
        // calculate the  new scores of the current alignment.
        calculate_score();
        // make the distance matrix with the new scores
        let distanceMatrix = makeDistanceMatrix(labels);
        // run the UPGMA algorithm with the new distance matrix
        let tree_data_newick_format = UPGMA(distanceMatrix, labels);
        // run an external script that converts a the newick format to a json format. This format can be used buy treant to make trees
        let tree_data_json_format = Newick.parse(tree_data_newick_format[0]);


        //document.getElementById('newick').innerHTML = tree_data_newick_format;
        // generate a new tree
        new Treant(tree_data_json_format);
    }

    /**
     * This loop adds divs to the html page that will contain each sequence and there pre en post dashes
     * it also makes the div with the sequence a draggable object and specifies the work for three events
     * start, drag, stop
     *
     * look at the event for a description of what they do
     *
     *
     */
    $.each(seqs, function (key, value) {
        $("#seqs").append("<div id='parent_" + key + "' class='parrent'>");
        $("#parent_" + key).append("<div id='Gap_pre_" + key + "' class='gap_pre'></div>");
        $("#parent_" + key).append("<div id=" + key + " class = seq>" + value["seq"] + "</div>");
        $("#parent_" + key).append("<div id='Gap_post_" + key + "' class='gap_post'></div></div></br>");

        // Set some base properties (only once)
        seqs[key]['length'] = value['seq'].length;
        seqs[key]['styled'] = value['seq'].split('');
        $("#" + key).draggable({
            grid: [gridsize],
            axis: "x",
            containment: $("#parent_" + key),
            /**
             * runs at the startup of the the creation of a drageble
             */
            start: function () {
                sequence = $(this);
            },
            /**
             * runs when a drag event is triggerd,
             * the fist bit is to make sure that the work is only done when a significant drag event has taken place
             *
             * if a significant drag event takes place it remove the previous HTML styling.
             * runs the prefiz function: for more information look at the function description.
             * and finally run the work function: for more information look at the function description.
             */
            drag: function () {
                let offset = sequence.offset();
                let xPos = offset.left;
                let a = (xPos - standaardAfwijking) / gridsize;

                // new significant drag event
                if (a !== seqs[key]['pos']) {
                    // Remove all previous styling
                    $.each(seqs, function (key, value) {
                        $("#" + key).html(html_strip($("#" + key).html()));
                        seqs[key]['styled'] = value['seq'].split('');
                        postfix($("#" + key));
                    });
                    // Add prefix and postfix dashes
                    prefix(sequence);
                    // Add styling based on matches and display consensus sequence (highlighted)
                    seqs[key]['pos'] = a;
                    Work();

                }
            },

            /**
             * runs when the mouse button is released this has been inplemented in order to make sure that it clears any
             * wrong alignment or styling du to a overload on drag events.
             *
             * remove the previous HTML styling.
             * runs the prefiz function: for more information look at the function description.
             * and finally run the work function: for more information look at the function description.
             */
            stop: function () {
                prefix(sequence);
                // Remove all previous styling
                $.each(seqs, function (key, value) {
                    $("#" + key).html(html_strip($("#" + key).html()));
                    seqs[key]['styled'] = value['seq'].split('');
                    postfix($("#" + key));
                });
                // Add styling based on matches and display consensus sequence (highlighted)
                Work();
            }
        });
        // Setting initial alignment
        postfix($("#" + key));
        Work();

    });

});